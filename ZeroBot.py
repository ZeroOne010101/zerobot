from __future__ import print_function
import discord
from discord.ext import commands
import asyncio
import datetime
import SECRETS
from itertools import cycle

# google imports

# from __future__ import print_function 	#needs to be on top
#from __future__ import print_function
import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

prefix = '.'
bot = commands.Bot(command_prefix = prefix)

#channel id's
notification_channel = "475377946635272193"



#defining stuff beforehand
players = {}
gcal_output_start = []
gcal_output_summary = []
gcal_checkrepeat_value = 16*60



#check for ready
@bot.event
async def on_ready():
    print ("Bot is ready.")




@bot.event
async def recurring_gcal():
	await bot.wait_until_ready()
	

	while not bot.is_closed:
		# #authorisation and build

		# If modifying these scopes, delete the file token.pickle.
		SCOPES = ['https://www.googleapis.com/auth/calendar.readonly']


		creds = None
		# The file token.pickle stores the user's access and refresh tokens, and is
		# created automatically when the authorization flow completes for the first
		# time.
		if os.path.exists('token.pickle'):
			with open('token.pickle', 'rb') as token:
				creds = pickle.load(token)
		# If there are no (valid) credentials available, let the user log in.
		if not creds or not creds.valid:
			if creds and creds.expired and creds.refresh_token:
				creds.refresh(Request())
			else:
				flow = InstalledAppFlow.from_client_secrets_file(
					'credentials.json', SCOPES)
				creds = flow.run_local_server()
			# Save the credentials for the next run
			with open('token.pickle', 'wb') as token:
				pickle.dump(creds, token)

		service = build('calendar', 'v3', credentials=creds)

		# Call the Calendar API

		#reset lists
		gcal_output_start = []
		gcal_output_summary = []

		# timestuff
		now_string = datetime.datetime.now().isoformat() + 'Z' # 'Z' indicates UTC time
		now_timedata = datetime.datetime.now()
		futurenow_timedata = now_timedata + datetime.timedelta(minutes=gcal_checkrepeat_value+1)
		futurenow_string = futurenow_timedata.isoformat() + 'Z'

		# print('Getting the upcoming events between '+ now_string + ' and ' + futurenow_string)
		events_result = service.events().list(calendarId='hmlii7fqloffqvvb4ci0vrrdak@group.calendar.google.com', timeMin=now_string,
											timeMax=futurenow_string, singleEvents=True,
											orderBy='startTime').execute()
		events = events_result.get('items', [])

		# if not events:
		# 	# print('No upcoming events found.')
		for event in events:
			start = event['start'].get('dateTime', event['start'].get('date'))
			# print(event['summary'])
			# print(start)
			gcal_output_start.append(start)
			gcal_output_summary.append(event['summary'])
			# print("")
			# print(gcal_output_summary)
			# print(gcal_output_start)	
				

		# notifier 
		if len(gcal_output_start) or len(gcal_output_summary) > 0 :
			output_count = 0
			while True:
				# message_summary_content = str(gcal_output_summary[output_count])
				# message_summary_content = message_summary_content[]
				await bot.send_message(bot.get_channel(notification_channel), gcal_output_summary[output_count])
				await bot.send_message(bot.get_channel(notification_channel), gcal_output_start[output_count])
				output_count = output_count + 1
				if output_count == len(gcal_output_start):
					break

		await asyncio.sleep(gcal_checkrepeat_value)



@bot.command()
async def test():
	await bot.say ('Test sucessful')

@bot.command()
async def echo(*args):
	output = ''
	for word in args:
		output += word
		output += ' '
	await bot.say(output)

@bot.command(pass_context=True)
async def play(ctx, url):
	server = ctx.message.server
	isConnected = bot.is_voice_connected(server)
	if isConnected == False:
		channel = ctx.message.author.voice.voice_channel
		await bot.join_voice_channel (channel)
	
	voice_client = bot.voice_client_in(server)
	ytdl_player = await voice_client.create_ytdl_player(url)
	players[server.id] = ytdl_player
	ytdl_player.start()

#print (player)
#print(type(player))                

@bot.command(pass_context=True)
async def pause(ctx):
	id = ctx.message.server.id
	players[id].pause()

@bot.command(pass_context=True)
async def resume(ctx):
	id = ctx.message.server.id
	isPlaying = players[id].is_playing() 
	if (isPlaying == False): 
		players[id].resume()
	else: 
		await bot.say ('Im already playing.')
                
@bot.command(pass_context=True)
async def stop(ctx):
	id = ctx.message.server.id
	players[id].stop()

@bot.command(pass_context = True)
async def leave(ctx):
	server = ctx.message.server
	voice_client = bot.voice_client_in(server)
	await voice_client.disconnect()


bot.loop.create_task(recurring_gcal())
bot.run(SECRETS.TOKEN)