@bot.event
async def recurring_gcalendar():
	await bot.wait_until_ready()
	

	while not bot.is_closed:
		#authorisation and build

		SCOPES = ['https://www.googleapis.com/auth/calendar.readonly']
		creds = None
		
		# The file token.pickle stores the user's access and refresh tokens, and is
		# created automatically when the authorization flow completes for the first
		# time.
		if os.path.exists('token.pickle'):
			with open('token.pickle', 'rb') as token:
				creds = pickle.load(token)
		
		# If there are no (valid) credentials available, let the user log in.
		
		if not creds or not creds.valid:
			if creds and creds.expired and creds.refresh_token:
				creds.refresh(Request())
			else:
				flow = InstalledAppFlow.from_client_secrets_file(
					'D:\Dateien\Downloads\credentials.json', SCOPES)
				creds = flow.run_local_server()
			
			# Save the credentials for the next run
			
			with open('token.pickle', 'wb') as token:
				pickle.dump(creds, token)    

		#build dat shit!
		gcalendar = build ('calendar', 'v3', credentials=creds)
		#gets the raw eventdata for processing
		eventdata = gcalendar.events().list(calendarId="mm92.mueller@gmail.com", singleEvents=True).execute()

		count = 0
		summary = []
		string_dateTime = []
		output_sum = []
		list_items = eventdata.get("items")
		if list_items == None:
			return 
		for i in list_items:
			summary_data = i.get("summary")
			dateTime_data = i.get("start").get("dateTime")

			summary.append(summary_data)
			string_dateTime.append(dateTime_data)

			current_string_dateTime = string_dateTime[count]
			if "+02:00" in current_string_dateTime:
				clean_string_dateTime = current_string_dateTime.replace("+02:00", "+0200")#
			if "+01:00" in current_string_dateTime:                                       #Winter/Summertime-stuff
				clean_string_dateTime = current_string_dateTime.replace("+01:00", "+0100")#

			
			dateTime = datetime.strptime(clean_string_dateTime, "%Y-%m-%dT%H:%M:%S%z")

			now = time.strftime("%Y-%m-%d %H:%M:%S%z", time.gmtime())
			now = datetime.strptime(now, "%Y-%m-%d %H:%M:%S%z")
			
			offset = time.timezone if (time.localtime().tm_isdst == 0) else time.altzone
			offset / 60 / 60 * -1
			#print (offset)
			offset_sec_actual = offset *-1
			#print (offset_sec_actual)

			now = now + timedelta(seconds=offset_sec_actual)

			notificationwindow = now + timedelta(minutes=user_timedelta_var)
			
			#checks for debugging
			# print (now)
			# print (notificationwindow)
			# print (dateTime)
			# print (type(now))
			# print (type(dateTime))
			# print (now < dateTime)

			if dateTime <= notificationwindow and dateTime >= now:
				output_sum.append(summary[count])
				#to prevent an infinitely appending list that would happen otherwise
				if len(output_time) != len(output_sum):
					output_time.append(str(dateTime))
			print (string_dateTime)
			print (len(string_dateTime))

			if count-1 == len(string_dateTime):
				break
			count = count + 1
			
		# checks for debugging
		# print (output_sum)
		# print (output_time)
		# print ("this code ran sucessfully")

		i=0
		while True:
			try:
				print(output_sum[i])
			except IndexError:
				break

			# await bot.send_message(bot.get_channel(channel), output_sum[i])
			# await bot.send_message(bot.get_channel(channel), output_time[i])
			
			print (output_sum)
			print (output_time)

			if i == len(output_sum):
				break
			i = i+1
			
		# for i in output_time:
		# 	await bot.send_message(bot.get_channel(channel), output_sum[i])
		# 	await bot.send_message(bot.get_channel(channel), output_time[i])
		await asyncio.sleep(user_recheck_int)
'''
Output
['2016-06-19T08:00:00+02:00']
1
['2016-06-19T08:00:00+02:00', '2016-06-14T15:00:00+02:00']
2
['2016-06-19T08:00:00+02:00', '2016-06-14T15:00:00+02:00', '2016-06-21T08:00:00+02:00']
3
['2016-06-19T08:00:00+02:00', '2016-06-14T15:00:00+02:00', '2016-06-21T08:00:00+02:00', '2016-06-09T17:55:00+02:00']
4
['2016-06-19T08:00:00+02:00', '2016-06-14T15:00:00+02:00', '2016-06-21T08:00:00+02:00', '2016-06-09T17:55:00+02:00', '2016-06-09T18:30:00+02:00']
5
['2016-06-19T08:00:00+02:00', '2016-06-14T15:00:00+02:00', '2016-06-21T08:00:00+02:00', '2016-06-09T17:55:00+02:00', '2016-06-09T18:30:00+02:00', '2016-06-13T16:30:00+02:00']
6
['2016-06-19T08:00:00+02:00', '2016-06-14T15:00:00+02:00', '2016-06-21T08:00:00+02:00', '2016-06-09T17:55:00+02:00', '2016-06-09T18:30:00+02:00', '2016-06-13T16:30:00+02:00', '2016-11-05T15:00:00+01:00']
7
['2016-06-19T08:00:00+02:00', '2016-06-14T15:00:00+02:00', '2016-06-21T08:00:00+02:00', '2016-06-09T17:55:00+02:00', '2016-06-09T18:30:00+02:00', '2016-06-13T16:30:00+02:00', '2016-11-05T15:00:00+01:00', '2017-05-18T06:00:00+02:00']
8
['2016-06-19T08:00:00+02:00', '2016-06-14T15:00:00+02:00', '2016-06-21T08:00:00+02:00', '2016-06-09T17:55:00+02:00', '2016-06-09T18:30:00+02:00', '2016-06-13T16:30:00+02:00', '2016-11-05T15:00:00+01:00', '2017-05-18T06:00:00+02:00', '2017-09-02T10:00:00+02:00']
9
['2016-06-19T08:00:00+02:00', '2016-06-14T15:00:00+02:00', '2016-06-21T08:00:00+02:00', '2016-06-09T17:55:00+02:00', '2016-06-09T18:30:00+02:00', '2016-06-13T16:30:00+02:00', '2016-11-05T15:00:00+01:00', '2017-05-18T06:00:00+02:00', '2017-09-02T10:00:00+02:00', '2018-10-05T15:40:00+02:00']
10
['2016-06-19T08:00:00+02:00', '2016-06-14T15:00:00+02:00', '2016-06-21T08:00:00+02:00', '2016-06-09T17:55:00+02:00', '2016-06-09T18:30:00+02:00', '2016-06-13T16:30:00+02:00', '2016-11-05T15:00:00+01:00', '2017-05-18T06:00:00+02:00', '2017-09-02T10:00:00+02:00', '2018-10-05T15:40:00+02:00', '2018-11-13T19:00:00+01:00']
11
['2016-06-19T08:00:00+02:00', '2016-06-14T15:00:00+02:00', '2016-06-21T08:00:00+02:00', '2016-06-09T17:55:00+02:00', '2016-06-09T18:30:00+02:00', '2016-06-13T16:30:00+02:00', '2016-11-05T15:00:00+01:00', '2017-05-18T06:00:00+02:00', '2017-09-02T10:00:00+02:00', '2018-10-05T15:40:00+02:00', '2018-11-13T19:00:00+01:00', '2018-12-08T01:40:00+01:00']
12
['2016-06-19T08:00:00+02:00', '2016-06-14T15:00:00+02:00', '2016-06-21T08:00:00+02:00', '2016-06-09T17:55:00+02:00', '2016-06-09T18:30:00+02:00', '2016-06-13T16:30:00+02:00', '2016-11-05T15:00:00+01:00', '2017-05-18T06:00:00+02:00', '2017-09-02T10:00:00+02:00', '2018-10-05T15:40:00+02:00', '2018-11-13T19:00:00+01:00', '2018-12-08T01:40:00+01:00', '2019-01-18T15:30:00+01:00']
13
['2016-06-19T08:00:00+02:00', '2016-06-14T15:00:00+02:00', '2016-06-21T08:00:00+02:00', '2016-06-09T17:55:00+02:00', '2016-06-09T18:30:00+02:00', '2016-06-13T16:30:00+02:00', '2016-11-05T15:00:00+01:00', '2017-05-18T06:00:00+02:00', '2017-09-02T10:00:00+02:00', '2018-10-05T15:40:00+02:00', '2018-11-13T19:00:00+01:00', '2018-12-08T01:40:00+01:00', '2019-01-18T15:30:00+01:00', '2019-01-29T16:30:00+01:00']
14
['2016-06-19T08:00:00+02:00', '2016-06-14T15:00:00+02:00', '2016-06-21T08:00:00+02:00', '2016-06-09T17:55:00+02:00', '2016-06-09T18:30:00+02:00', '2016-06-13T16:30:00+02:00', '2016-11-05T15:00:00+01:00', '2017-05-18T06:00:00+02:00', '2017-09-02T10:00:00+02:00', '2018-10-05T15:40:00+02:00', '2018-11-13T19:00:00+01:00', '2018-12-08T01:40:00+01:00', '2019-01-18T15:30:00+01:00', '2019-01-29T16:30:00+01:00', '2019-01-28T17:30:00+01:00']
15
['2016-06-19T08:00:00+02:00', '2016-06-14T15:00:00+02:00', '2016-06-21T08:00:00+02:00', '2016-06-09T17:55:00+02:00', '2016-06-09T18:30:00+02:00', '2016-06-13T16:30:00+02:00', '2016-11-05T15:00:00+01:00', '2017-05-18T06:00:00+02:00', '2017-09-02T10:00:00+02:00', '2018-10-05T15:40:00+02:00', '2018-11-13T19:00:00+01:00', '2018-12-08T01:40:00+01:00', '2019-01-18T15:30:00+01:00', '2019-01-29T16:30:00+01:00', '2019-01-28T17:30:00+01:00', '2019-01-26T19:20:00+01:00']
16
['2016-06-19T08:00:00+02:00', '2016-06-14T15:00:00+02:00', '2016-06-21T08:00:00+02:00', '2016-06-09T17:55:00+02:00', '2016-06-09T18:30:00+02:00', '2016-06-13T16:30:00+02:00', '2016-11-05T15:00:00+01:00', '2017-05-18T06:00:00+02:00', '2017-09-02T10:00:00+02:00', '2018-10-05T15:40:00+02:00', '2018-11-13T19:00:00+01:00', '2018-12-08T01:40:00+01:00', '2019-01-18T15:30:00+01:00', '2019-01-29T16:30:00+01:00', '2019-01-28T17:30:00+01:00', '2019-01-26T19:20:00+01:00', '2019-01-27T21:00:00+01:00']
17
['2016-06-19T08:00:00+02:00', '2016-06-14T15:00:00+02:00', '2016-06-21T08:00:00+02:00', '2016-06-09T17:55:00+02:00', '2016-06-09T18:30:00+02:00', '2016-06-13T16:30:00+02:00', '2016-11-05T15:00:00+01:00', '2017-05-18T06:00:00+02:00', '2017-09-02T10:00:00+02:00', '2018-10-05T15:40:00+02:00', '2018-11-13T19:00:00+01:00', '2018-12-08T01:40:00+01:00', '2019-01-18T15:30:00+01:00', '2019-01-29T16:30:00+01:00', '2019-01-28T17:30:00+01:00', '2019-01-26T19:20:00+01:00', '2019-01-27T21:00:00+01:00', '2019-02-02T02:09:00+01:00']
18
Bot is ready.
'''